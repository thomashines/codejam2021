#!/usr/bin/env python3

import itertools
import sys

def util_gcd(a, b):
  afs = util_prime_factors(a)
  bfs = util_prime_factors(b)
  af = 1
  bf = 1
  result = 1
  while af is not None and bf is not None:
    if af == bf:
      result *= af
      af = next(afs, None)
      bf = next(bfs, None)
    elif af > bf:
      bf = next(bfs, None)
    elif af < bf:
      af = next(afs, None)
  return result

def util_lcm(a, b):
  return (a * b) // util_gcd(a, b)

def util_prime_factors(number, window_size=2 ** 5):
  if number == 1:
    yield 1
  else:
    for prime in util_primes(number, window_size=window_size):
      while number % prime == 0:
        yield prime
        number = number // prime
      if number == 1:
        break

PRIME_CACHE = [2]
def util_primes(max_prime=None, window_size=2 ** 10):
  # Repeat cache
  for prime in PRIME_CACHE:

    # Stop
    if max_prime is not None and prime > max_prime:
      break

    yield prime

  # Window of sieve of Eratosthenes
  window_offset = 0
  not_prime_window = [False] * window_size

  # Find primes
  for number in itertools.count(start=(PRIME_CACHE[-1] + 1)):

    # Stop
    if max_prime is not None and number > max_prime:
      break

    # Move the sieve window
    if number >= window_offset + window_size:
      window_offset = (number // window_size) * window_size
      for i in range(window_size):
        not_prime_window[i] = False
      for prime in PRIME_CACHE:
        multiple = (window_offset // prime) * prime
        while multiple < window_offset + window_size:
          if multiple >= window_offset:
            not_prime_window[multiple - window_offset] = True
          multiple += prime

    # Test if prime
    if not not_prime_window[number - window_offset]:
      yield number

      # Add to the sieve
      PRIME_CACHE.append(number)
      for multiple in itertools.count(start=2):
        this_not_prime = number * multiple
        if this_not_prime >= window_offset + window_size:
          break
        not_prime_window[this_not_prime - window_offset] = True

TRANSLATE = str.maketrans("FT", "01")
UNTRANSLATE = str.maketrans("01", "FT")

def main():
  input = tuple(line.strip()for line in sys.stdin)
  line_index = 1
  cases = int(input[0])
  case = 1
  while case <= cases:
    student_count, question_count = input[line_index].split(" ")
    student_count = int(student_count)
    question_count = int(question_count)
    line_index += 1
    case_last_line_index = line_index + student_count
    results = []
    while line_index < case_last_line_index:
      answers, score = input[line_index].split(" ")
      results.append((
        tuple(1 if (a == "T") else 0 for a in answers),
        int(score),
      ))
      line_index += 1
    results = tuple(results)
    trues = [0] * question_count
    possibilities = 0
    for possibility in itertools.product((0, 1), repeat=question_count):
      # print("possibility", possibility)
      is_possible = True
      for answers, score in results:
        matching_count = sum(1 if (answer == truth) else 0 for answer, truth in zip(answers, possibility))
        if matching_count != score:
          is_possible = False
          break
      if is_possible:
        # print("is possible")
        for question in range(question_count):
          trues[question] += possibility[question]
        possibilities += 1
    true_threshold = possibilities // 2
    best_answers = ""
    score_numerator = 0
    score_denominator = possibilities
    for question in range(question_count):
      is_true = trues[question] > true_threshold
      best_answers += "T" if is_true else "F"
      score_numerator += trues[question] if is_true else (possibilities - trues[question])
    gcd = util_gcd(score_numerator, score_denominator)
    score_numerator //= gcd
    score_denominator //= gcd
    print("Case #{}: {} {}/{}".format(case, best_answers, score_numerator, score_denominator))
    case += 1

if __name__ == "__main__":
  main()
