#!/usr/bin/env python3

import sys

def util_digits(number):
  count = 1
  while number >= 10:
    count += 1
    number //= 10
  return count

def main():
  input = tuple(line.strip()for line in sys.stdin)
  cases = int(input[0])
  for case in range(1, cases + 1):
    size = int(input[2 * case - 1])
    numbers = list(int(number) for number in input[2 * case].split(" "))
    operations = 0
    previous_number = None
    for next_number in numbers:
      if previous_number is None or previous_number < next_number:
        previous_number = next_number
      else:
        previous_digits = util_digits(previous_number)
        next_digits = util_digits(next_number)
        added_digits = previous_digits - next_digits
        multor = 10 ** added_digits
        next_number *= multor
        difference = previous_number - next_number
        if 0 <= difference < (multor - 1):
          next_number += difference + 1
        elif (multor - 1) <= difference:
          next_number *= 10
          added_digits += 1
        operations += added_digits
        previous_number = next_number
    print("Case #{}: {}".format(case, operations))

if __name__ == "__main__":
  main()
