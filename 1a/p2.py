#!/usr/bin/env python3

import sys

def util_product(values):
  result = 1
  for value in values:
    result = result * value
  return result

def main():
  input = tuple(line.strip()for line in sys.stdin)
  line_index = 1
  cases = int(input[0])
  case = 1
  while case <= cases:
    uniques = int(input[line_index])
    line_index += 1
    case_last_line_index = line_index + uniques
    cards_counts = dict()
    while line_index < case_last_line_index:
      card, count = input[line_index].split(" ")
      cards_counts[int(card)] = int(count)
      line_index += 1
    cards = tuple(sorted(cards_counts.keys()))
    cards_indices = dict(((card, index) for index, card in enumerate(cards)))
    total_counts = tuple(cards_counts[card] for card in cards)
    selection = list(total_counts)
    selection_sum = sum(card * count for card, count in zip(cards, total_counts))
    selection_product = 1
    max_score = 0
    done = False
    while not done:
      for index in range(uniques + 1):
        if index == uniques:
          done = True
          break
        elif selection[index] > 0:
          selection[index] -= 1
          selection_sum -= cards[index]
          selection_product *= cards[index]
          break
        else:
          selection[index] = total_counts[index]
          selection_sum += cards[index] * total_counts[index]
          selection_product //= cards[index] ** total_counts[index]
      if done:
        break
      # print("selection", selection)
      # print("selection_sum",
      #       selection_sum,
      #       sum((card * count) for card, count in zip(cards, selection)))
      # print("selection_product",
      #       selection_product,
      #       util_product(((card ** (total - count) for card, total, count in zip(cards, total_counts, selection)))))
      # print(selection_sum, selection_product)
      max_score = max(max_score, selection_sum if (selection_sum == selection_product) else 0)
    print("Case #{}: {}".format(case, max_score))
    case += 1

if __name__ == "__main__":
  main()
