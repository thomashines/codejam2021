#!/usr/bin/env python3

import sys

# N: number of elements to sort
# C: goal cost of sorting
# Best case: input is already sorted, cost is N - 1
# Worst case: max number of elements are reversed each step, which is
# ((N - 0) + (N - 1) + (N - 2) + ... + (N - N))
# To go from best case to best case + 1 reverse the order of the last 2 elements
# Iteration K can add a cost between 1 and N - K
# Just be greedy

# The min cost for the first iterations for a list of the given length
def min_cost(length, iterations):
  return iterations

# The max cost for the first iterations for a list of the given length
def max_cost(length, iterations):
  cost = 0
  while iterations > 0:
    cost += length - (iterations - 1)
    iterations -= 1
  return cost

def main():
  input = tuple(line.strip()for line in sys.stdin)
  cases = int(input[0])
  for case in range(1, cases + 1):
    case_input = input[case]
    length, cost = case_input.split(" ")
    length = int(length)
    cost = int(cost)
    values = tuple(range(1, length + 1))
    # Do iterations in reverse
    remaining_cost = cost
    impossible = False
    for iteration in range(length - 1):
      # This iteration is considering the range
      #   values[length - iteration - 2:]
      # The range of costs that must be added:
      #   min: if all subsequent iterations add their max cost, how much must be
      #        done in this step?
      #   max: if all subsequent iterations add their min cost, how much must be
      #        done in this step?
      #   subsequent min: 1 for each subsequent iteration
      #   subsequent max:
      # subsequent_min = length - iteration
      min_added_cost = 1
      max_added_cost = iteration + 2
      min_required_cost = remaining_cost - max_cost(length, length - iteration - 2)
      max_required_cost = remaining_cost - min_cost(length, length - iteration - 2)
      if max_required_cost < min_added_cost:
        # Target cost is impossibly small
        impossible = True
        break
      elif max_added_cost < min_required_cost:
        # Target cost is impossibly large
        impossible = True
        break
      else:
        # Add the most possible cost
        add_cost = min(max_added_cost, max_required_cost)
        remaining_cost = remaining_cost - add_cost
        values = (values[:length - iteration - 2] +
                  values[length - iteration - 2:length - iteration - 2 + add_cost][::-1] +
                  values[length - iteration - 2 + add_cost:])
    print("Case #{}: {}".format(case, "IMPOSSIBLE" if impossible else " ".join(map(str, values))))

if __name__ == "__main__":
  main()
