#!/usr/bin/env python3

import sys

def reversort_brute(values):
  lowest_index = None
  lowest_value = None
  for index, value in enumerate(values):
    if lowest_value is None or value < lowest_value:
      lowest_index = index
      lowest_value = value
  next_values = values[:lowest_index + 1][::-1] + values[lowest_index + 1:]
  cost = lowest_index + 1
  if len(next_values) > 2:
    cost += reversort_brute(next_values[1:])
  return cost

def main():
  input = tuple(tuple(int(value) for value in line.strip().split(" ")) for line in sys.stdin)
  cases = input[0][0]
  for case in range(1, cases + 1):
    case_input = input[2 * (case - 1) + 1:2 * (case - 1) + 3]
    length = case_input[0][0]
    original = case_input[1]
    cost = reversort_brute(original)
    print("Case #{}: {}".format(case, cost))

if __name__ == "__main__":
  main()
