#!/usr/bin/env python3

import sys

def main():
  input = tuple(line.strip() for line in sys.stdin)
  cases = int(input[0])
  for case in range(1, cases + 1):
    case_input = input[case]
    cost_cj, cost_jc, mural = case_input.split(" ")
    cost_cj = int(cost_cj)
    cost_jc = int(cost_jc)
    cost = 0
    previous_character = None
    for character in mural:
      if previous_character == "C" and character == "J":
        cost += cost_cj
      elif previous_character == "J" and character == "C":
        cost += cost_jc
      if character != "?":
        previous_character = character
    print("Case #{}: {}".format(case, cost))

if __name__ == "__main__":
  main()
